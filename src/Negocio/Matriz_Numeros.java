/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

/**
 * Modelamiento de una matriz usando el concepto de vector de vectores
 *
 * @author madarme
 */
public class Matriz_Numeros {

    private ListaNumeros[] filas;

    public Matriz_Numeros() {
    }

    /**
     * Crea una matriz a partir de una cadena formateada: elemento1,
     * elemento2... ; elemento.....; .... Cada elemento separado por una "," y
     * cada fila por un ";"
     *
     * @param cadena un String que contiene los datos de la matriz
     */
    public Matriz_Numeros(String cadena) {
        if (cadena.isEmpty()) {
            throw new RuntimeException("No se puede cargar la matriz, cadena vacía");
        }

        //1. Crear los datos para la filas:
        /**
         * http://javainutil.blogspot.com/2013/04/java-manejo-de-cadenas-metodo.html#:~:text=M%C3%A9todo%20String.-,split().,o%20una%20construcci%C3%B3n%20mas%20elaborada.
         *
         * https://www.youtube.com/watch?v=ypx4_BHpjUc
         */
        String filaDatos[] = cadena.split(";");

        //2. Crear las filas:
        this.filas = new ListaNumeros[filaDatos.length];
        for (int i = 0; i < filaDatos.length; i++) {
            String columnaDatos[] = filaDatos[i].split(",");
            ListaNumeros nuevaColumnas = new ListaNumeros(columnaDatos.length);
            this.pasarDatosColumna(nuevaColumnas, columnaDatos);
            //Ingresar la columna a la matriz:
            this.filas[i] = nuevaColumnas;

        }

    }

    private void pasarDatosColumna(ListaNumeros col, String datos[]) {

        for (int j = 0; j < datos.length; j++) {
            col.adicionar(j, Float.parseFloat(datos[j]));

        }
    }

    /**
     * Puedo utilizarlo para matrices dispersa
     *
     * @param cantFilas
     */
    public Matriz_Numeros(int cantFilas) {
        if (cantFilas <= 0) {
            throw new RuntimeException("No se puede crear la matriz , sus cantidad de filas debe ser mayor a 0");
        }
        this.filas = new ListaNumeros[cantFilas];
    }

    /**
     * Creación de matrices cuadradas o bien rectangulares
     *
     * @param cantFilas
     * @param cantColumnas
     */
    public Matriz_Numeros(int cantFilas, int cantColumnas) {
        if (cantFilas <= 0 || cantColumnas <= 0) {
            throw new RuntimeException("No se puede crear la matriz , sus cantidad de filas o columnas debe ser mayor a 0");
        }
        this.filas = new ListaNumeros[cantFilas];
        //Creando columnas:
        for (int i = 0; i < cantFilas; i++) {
            this.filas[i] = new ListaNumeros(cantColumnas);
        }
 
    }

    public ListaNumeros[] getFilas() {
        return filas;
    }

    public void setFilas(ListaNumeros[] filas) {
        this.filas = filas;
    }

    private void validar(int i) {
        if (i < 0 || i >= this.filas.length) {
            throw new RuntimeException("Índice fuera de rango para una fila:" + i);
        }
    }

    public void adicionarVector(int i, ListaNumeros listaNueva) {
        this.validar(i);
        this.filas[i] = listaNueva;
    }

    /**
     * Esté mètodo funciona SI Y SOLO SI LA MATRIZ ESTÁ CREADA CON FILAS Y
     * COLUMNAS
     *
     * @param i índice de la fila
     * @param j índice de la columna
     * @param nuevoDato dato a ingresar en i,j
     */
    public void setElemento(int i, int j, float nuevoDato) {

    }

    @Override
    public String toString() {
        String msg = "";

        for (ListaNumeros myLista : this.filas) {
            msg += myLista.toString() + "\n";
        }
        return msg;
    }

    public int length_filas() {
        return this.filas.length;
    }

    /**
     * Obtiene que tipo de matriz es: Cuadrada, rectangular o dispersa
     *
     * @return una cadena con el tipo de matrizular o dispersa
     */
    public String getTipo() {

        ListaNumeros col; // se crea el vector columnas
        col  = this.filas[0];//se iguala al vector de la posicion 0 de la matriz
        int aux = col.length();//se le asigna el tamaño del vector columnas
        int c=0;
        
        for (int i = 0; i < this.length_filas(); i++) {
           col = this.filas[i];// se iguala al vector de la posicion i de la matriz
           if (this.length_filas() == col.length()){
            c++;   
           }
           if (col.length() != aux){
            return "Es dispersa";
           }
        }
        if (c != 0){
            return "cuadrada";
        }else {
            return "Rectangular";
        }
        
    }
}
